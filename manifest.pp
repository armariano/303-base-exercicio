$svc_conf_template = @(END)
[Unit]
Description=Api de Investimento

[Service]
ExecStart=/usr/bin/java -jar /home/ubuntu/api-investimentos/Api-Investimentos-0.0.1-SNAPSHOT.jar

[Install]
WantedBy=multi-user.target
END

$key_pair_jenkins = @(END)
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDvNfe5q8SfXeGyl6cB2vR4mo7pHq30sjPgMrhP05UF2gH3bgz7dGREpb6caCGJo8gpQXofeEI0wdqLw0KbmIuqSMpT4yQQHWr6VqWbDaK2XQutHJkElGQMTCBADwT4bGt8b2eNV8s581g/RS01q4L7F9iSkMzSms3BM63FkZHZAOlOTHmRnN3Ju39MolmjGPwb6SpUFYpMIZLic1vKTe4p1L3PQL4czIJxpssE283iOeRiAceD/Xaly4mgVqdxmMEm3gZo5PzcPMu5Y08M6Ur0vJR5i4IVSSheBhzcKLshuvTBt5bobYtG1BWatQIpbKoCO6Uqnf/7U7FvPHJTYsJb jenkins@banana
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCirAsV9q368e0drEk4iBlM1UpgDz1vF6sliavXz7ajJYDkTojTHXOHp6iZ/blbiyvhu5ScKzj8h5NnL7Upso+yEzFkqrMPN7lRbRaQJrLGrmcSBrs/QVh622LMU8CnZZ2qKsBv7ETp/lTs9NKTkCn9s2SrW3xdNDvIJHn8CV82/4sVBMBYxfzN6cEL5lHAGKePPZ3Sny45tlax/HOT7EUQ38N+xhvrbWAOk+S0aCi0TAhwGY+EkdyIacRzciF8SetOro3GDa3Aqf7URUE0Bg8kuSfHwkpHab/kUn5lBDzJvgj7WxFDnce5A3L571jbyAwzeXMtJAFaCo4Ck6Y/xgvX alder-keypair
    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC/22H8qr8+HcJp9MVxnUIu5aFPWFLjOSeoEEkG4rp56yw0MFSFFY6IjK4hPV/vlUcSpuE/PhyGETFba1XIBwGMWjSA6rWwYja3NNS6EqBlUi5+RIaH+d6zimDsa2dnpipe30r8Qo5XkORuuiO5IAY5x1SxoaicHFMOnKbES0RlUN4kuhxwRaQr4JHkAgKvutX+hbGCMoyZev8ijyujkQa2seVw5YjD9puS45mZ3j/FXHoMDTmJjUwqBw0dFuDrit9ouoBE7ZSgutJjfv9XiGFBRnKJhiEzE6joruzOEpYhsdG/0fkkVinzCM8E2GFGI60A0ZPTYLnROOdZmVyNHJyTdtXTtuEhK4dCsWGlAnSH4Bi5IuvGMAzgNGTGmnzgVgIQ58NAtC+hYE+Gq1qasrCPj2XTwxKG9H0DstqtDf0J+wJRFvarAvmaNS1kkjSQosX+GFhaEG10DhNHIIUGacZRFeaSGtHheCR0/xB+OQOmPReaSTni2+XciIAwn551KpU= a2w@localhost.localdomain
END

class { 'java':
    package => 'openjdk-8-jdk',
}
file { '/home/ubuntu/api-investimentos':
    ensure => 'directory',
    owner  => 'ubuntu',
    group  => 'ubuntu',
    mode   => '0777',
}
file { '/home/ubuntu/.ssh/authorized_keys':
    ensure => file,
    content => inline_epp($key_pair_jenkins),
}
file { '/etc/systemd/system/api-investimentos.service':
    ensure => file,
    content => inline_epp($svc_conf_template),
}
service { 'api-investimentos':
    ensure => running,
}
class{"nginx": }
    nginx::resource::server { 'api-investimentos':
    server_name => ['_'],
    listen_port => 80,
    proxy => 'http://localhost:8080',
}
file { "/etc/nginx/conf.d/default.conf":
    ensure => absent,
    notify => Service['nginx'],
}
